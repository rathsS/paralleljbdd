<!--
SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# PJBDD Developers Documentation
PJBDD uses [Ant Ivy](https://ant.apache.org/ivy/) dependency manager. 


## Style Guide

The style guide of this project is the Google Java Style:
https://google.github.io/styleguide/javaguide.html

We use the auto-formatting tool, hence before committing run `ant format-source` in order to
format the entire project.

Additionally, refer to the CPAchecker
[style guide](https://github.com/sosy-lab/cpachecker/blob/trunk/doc/StyleGuide.txt)
for more information.

## Continuous Integration

We rely on [GitLab-CI][https://docs.gitlab.com/ee/ci/] for continuous integration, which picks up code style violations,
compile warnings for both ECJ and javac (for several versions of Java),
[SpotBugs](https://github.com/spotbugs/spotbugs) errors,...

## Releasing PJBDD

Currently, releases are pushed to [Ivy Repository][].
The release version number is derived from the `git describe` command,
which output is either a git tag (if the release version corresponds exactly
to the tagged commit), or a latest git tag together with a distance measured
in number of commits and a git hash corresponding to the current revision.

### Creating new release numbers

New PJBDD version is defined by creating a new git tag with a version number.
Git tags should be signed (`git tag -s` command).
When creating a new version number, populate the `CHANGELOG.md` file with the
changes which are exposed by the new release.

[Semantic versioning][] guidelines should be followed when defining a new
version.

### Release to Ivy

If you have write permission to the [Ivy Repository][] you can perform the
release as follows:

 - Symlink the `repository` folder in the PJBDD to the root of the SVN
    checkout of the Ivy repository.
 - Run the `publish` ANT task.
 - Manually perform the commit in SVN.
