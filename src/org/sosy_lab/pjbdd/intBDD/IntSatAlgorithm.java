// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.cache.Cache;

/**
 * Int based sat algorithm implementation. Uses serial algorithms for bdd sat operations.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public class IntSatAlgorithm {

  /** the node manager instance. */
  private final IntNodeManager nodeManager;
  /** sat count cache for computed results. */
  private final Cache<Integer, BigInteger> satCache;

  /**
   * Creates new {@link IntSatAlgorithm} instances with.
   *
   * @param nodeManager - the node managing component
   * @param satCache - the sat cache
   */
  public IntSatAlgorithm(IntNodeManager nodeManager, Cache<Integer, BigInteger> satCache) {
    this.nodeManager = nodeManager;
    this.satCache = satCache;
  }

  /**
   * Takes a bdd and finds any satisfying variable assignment.
   *
   * @param root - the bdd node
   * @return one satisfying variable assignment.
   */
  public int anySat(int root) {
    if (isConst(root)) {
      return root;
    }
    if (nodeManager.getFalse() == low(root)) {
      return nodeManager.makeNode(var(root), nodeManager.getFalse(), anySat(high(root)));
    } else {
      return nodeManager.makeNode(var(root), anySat(low(root)), nodeManager.getFalse());
    }
  }

  /**
   * Get {@link DD}'s number of satisfying variable assignments. All created variables are
   * considered.
   *
   * @param root - the {@link DD} node
   * @return the number of satisfying variable assignments.
   */
  public BigInteger satCount(int root) {
    return BigInteger.valueOf(2).pow(level(root)).multiply(satCountRec(root));
  }

  /**
   * Recursive computation of bdd's number of possible satisfying truth assignments.
   *
   * @param root - the bdd
   * @return number of possible satisfying truth assignments
   */
  private BigInteger satCountRec(int root) {
    if (isConst(root)) {
      return BigInteger.valueOf(root);
    }

    BigInteger cached = satCache.get(root);
    if (cached != null) {
      return cached;
    }

    if (low(root) == -1) {
      System.out.println("low: " + root);
    }

    if (high(root) == -1) {
      System.out.println("high: " + root);
    }
    BigInteger s = BigInteger.valueOf(2).pow((level(low(root)) - level(root) - 1));
    BigInteger size = s.multiply(satCountRec(low(root)));

    s = BigInteger.valueOf(2).pow((level(high(root)) - level(root) - 1));
    size = size.add(s.multiply(satCountRec(high(root))));

    satCache.put(root, size);
    return size;
  }

  /**
   * helper call to determine bdd's variable level.
   *
   * @param root - the bdd
   * @return level of root
   */
  private int level(int root) {
    return nodeManager.level(var(root));
  }
  /**
   * helper call to determine bdd's variable.
   *
   * @param root - the bdd
   * @return variable of root
   */
  private int var(int root) {
    return nodeManager.var(root);
  }
  /**
   * helper call to determine bdd's high branch successor.
   *
   * @param root - the bdd
   * @return high successor of root
   */
  private int high(int root) {
    return nodeManager.high(root);
  }
  /**
   * helper call to determine bdd's low branch successor.
   *
   * @param root - the bdd
   * @return low successor of root
   */
  private int low(int root) {
    return nodeManager.low(root);
  }
  /**
   * helper call to determine if root is a terminal node.
   *
   * @param root - the bdd
   * @return is root terminal node
   */
  private boolean isConst(int root) {
    return root == nodeManager.getFalse() || root == nodeManager.getTrue();
  }
}
