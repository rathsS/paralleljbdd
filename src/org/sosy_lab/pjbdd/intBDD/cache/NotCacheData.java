package org.sosy_lab.pjbdd.intBDD.cache;

/** Table Entry class. */
public class NotCacheData {

  private int input;
  private int result;

  public NotCacheData(int input, int result) {
    this.input = input;
    this.result = result;
  }

  public int getInput() {
    return input;
  }

  public void setInput(int input) {
    this.input = input;
  }

  public int getResult() {
    return result;
  }

  public void setResult(int result) {
    this.result = result;
  }
}
