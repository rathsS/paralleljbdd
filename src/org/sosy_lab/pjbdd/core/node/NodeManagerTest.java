package org.sosy_lab.pjbdd.core.node;

import org.junit.Assert;
import org.sosy_lab.pjbdd.test.CreatorCombinatorTest;

public class NodeManagerTest extends CreatorCombinatorTest {

  @Override
  public void test() {
    initialVarCountCanBeIncreasedWithIthVarDuringRuntime();
    initialVarCountCanBeIncreasedWithMakeVarDuringRuntime();
  }

  private void initialVarCountCanBeIncreasedWithIthVarDuringRuntime() {
    int initialVarcount = creator.getVariableCount();

    for (int i = 0; i < initialVarcount + 1; i++) {
      creator.makeIthVar(i);
    }

    Assert.assertEquals(initialVarcount + 1, creator.getVariableCount());

    initialVarcount = creator.getVariableCount();

    for (int i = 0; i < initialVarcount * 2; i++) {
      creator.makeIthVar(i);
    }

    Assert.assertEquals(initialVarcount * 2, creator.getVariableCount());
  }

  private void initialVarCountCanBeIncreasedWithMakeVarDuringRuntime() {
    int initialVarcount = creator.getVariableCount();

    creator.makeVariable();

    Assert.assertEquals(initialVarcount + 1, creator.getVariableCount());

    initialVarcount = creator.getVariableCount();

    for (int i = 0; i < initialVarcount; i++) {
      creator.makeVariable();
    }

    Assert.assertEquals(initialVarcount * 2, creator.getVariableCount());
  }
}
