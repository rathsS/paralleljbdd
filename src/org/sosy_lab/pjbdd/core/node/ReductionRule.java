// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.node;

import java.util.Optional;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Interface for decision diagram types specific reduction rules.
 *
 * @param <T> DD type
 * @author Stephan Holzner
 * @since 1.2
 */
public interface ReductionRule<T extends DD> {

  /**
   * Applies the dd specific reduction rule on low, high and var.
   *
   * @param low - the low branch argument.
   * @param high - the high branch argument.
   * @param var - the variable argument.
   * @return {@link Optional#of} the reduced dd or {@link Optional#empty()} if the rule can not be
   *     applied.
   */
  Optional<T> apply(T low, T high, int var);
}
