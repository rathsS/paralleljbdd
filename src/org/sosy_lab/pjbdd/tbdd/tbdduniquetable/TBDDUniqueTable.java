package org.sosy_lab.pjbdd.tbdd.tbdduniquetable;

import java.util.function.Consumer;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.bdd.BDDCreator;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDDNode;

/**
 * Main {@link TBDD} based unique table interface defining all unique table methods. Used in {@link
 * BDDCreator} implementations.
 *
 * @author Stephan Holzner
 * @see BDDCreator
 * @since 1.0
 */
public interface TBDDUniqueTable {

  /** Clear all unique table entries. */
  void clear();

  /**
   * Get existing or else create the {@link TBDD} with following attributes.
   *
   * @param tbdd - the tbdd
   * @return the matching bdd
   */
  TBDDNode getOrCreateTBDD(TBDDNode tbdd);

  /**
   * Get the logical true {@link TBDD} representation.
   *
   * @return logical true representation
   */
  TBDDNode getTrue();

  /**
   * Get the logical false {@link TBDD} representation.
   *
   * @return logical false representation
   */
  TBDDNode getFalse();

  /** Shutdown unique table: cleanup resources. */
  void shutDown();

  /**
   * Apply function on each bdd in table.
   *
   * @param function - the function
   */
  void forEach(Consumer<? super DD> function);

  /**
   * Get the table's bdd node factory.
   *
   * @return the node factory
   */
  DD.Factory<DD> getFactory();
}
