/*
 *  Parallel JBDD is A parallel BDD library.
 *  This file is part of Parallel JBDD.
 *
 *  Copyright (C) 2007-2021  Dirk Beyer
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.sosy_lab.pjbdd.tbdd.taggedDD;

import org.sosy_lab.pjbdd.core.uniquetable.UniqueTable;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;

public interface TaggedDDUniqueTable extends UniqueTable<TBDD> {

  TBDD getOrCreate(TBDD low, TBDD high, int var, int tag);
}
