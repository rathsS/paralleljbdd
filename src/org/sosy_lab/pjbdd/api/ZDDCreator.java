// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import java.math.BigInteger;
import java.util.List;
import org.sosy_lab.pjbdd.zdd.ZDDCreatorImpl;

/**
 * Main zdd boolean_operations interface defining the following operations.
 *
 * <p>Implementations are:
 *
 * <ul>
 *   <li>{@link ZDDCreatorImpl}
 * </ul>
 *
 * @author Stephan Holzner
 * @see ZDDCreatorImpl
 * @since 1.0
 */
public interface ZDDCreator {

  /**
   * Creates a {@link DD} representing the high subset of an zdd with restricting a given variable
   * to 1.
   *
   * @param zdd - given zdd
   * @param var - {@link DD} representation of given variable
   * @return (subset of zdd with var equals ONE)
   */
  DD subSet1(DD zdd, DD var);

  /**
   * Creates a {@link DD} representing the high subset of an zdd with restricting a given variable
   * to 0.
   *
   * @param zdd - given zdd
   * @param var - {@link DD} representation of given variable
   * @return (subset of zdd with var equals ZERO)
   */
  DD subSet0(DD zdd, DD var);

  /**
   * Creates a {@link DD} with inverted variable.
   *
   * @param zdd - given zdd
   * @param var - {@link DD} representation of given variable
   * @return (invert variable var for zdd)
   */
  DD change(DD zdd, DD var);

  /**
   * Creates a zdd representing the union set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 UNION zdd2)
   */
  DD union(DD zdd1, DD zdd2);

  /**
   * Creates a zdd representing the difference set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 DIFFERENCE zdd2)
   */
  DD difference(DD zdd1, DD zdd2);

  /**
   * Creates a zdd representing the intersection set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 INTERSECT zdd2)
   */
  DD intersection(DD zdd1, DD zdd2);

  /**
   * Creates a zdd representing the product product set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 { @ literal * } zdd2)
   */
  DD product(DD zdd1, DD zdd2);

  /**
   * Creates a zdd representing the remainder set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 { @ literal % } zdd2)
   */
  DD modulo(DD zdd1, DD zdd2);

  /**
   * Creates a zdd representing the quotient set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 { @ literal / } zdd2)
   */
  DD division(DD zdd1, DD zdd2);

  /**
   * Creates a zdd representing the exclusion set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 EXCLUDE zdd2)
   */
  DD exclude(DD zdd1, DD zdd2);

  /**
   * Creates a zdd representing the restriction set of two arguments.
   *
   * @param zdd1 - getIf {@link DD} argument
   * @param zdd2 - getThen {@link DD} argument
   * @return (zdd1 RESTRICT zdd2)
   */
  DD restrict(DD zdd1, DD zdd2);

  /**
   * Returns existing {@link DD} node with given low, high branches and variable. Creates a new one
   * if there is no existing.
   *
   * @param low - the low branch
   * @param high - the high branch
   * @param var - the bdd variable
   * @return new or existing bdd
   */
  DD makeNode(DD low, DD high, int var);

  /**
   * Get the representation of empty set.
   *
   * @return empty set
   */
  DD empty();

  /**
   * Get the representation of the base set.
   *
   * @return base set
   */
  DD base();

  /**
   * Get the universal set representation.
   *
   * @return universal set
   */
  DD universe();

  /**
   * creates a cube of a variable amount of input variables. Example: Ordering = x1,x2,x3,x4 ...
   * Cube of: x1, x3 Use: cube(1,0,1).
   *
   * @param vars - input variables
   * @return cube of input variables
   */
  DD cube(int... vars);

  /**
   * Get {@link DD}'s number of satisfying variable assignments. All created variables are
   * considered.
   *
   * @param bdd - the {@link DD} node
   * @return the number of satisfying variable assignments.
   */
  BigInteger satCount(DD bdd);

  /** Shutdown boolean_operations and cleanup resources. */
  void shutdown();

  /**
   * Sets the bdd variable ordering and updates all to the new ordering.
   *
   * @param pOrder - the new order of the variables.
   */
  void setVarOrder(List<Integer> pOrder);

  /**
   * Set number of existing variables.
   *
   * @param count - the new variable count
   */
  void setVariableCount(int count);
}
