// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import org.junit.Assert;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'NAND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'NAND'
 * test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class NandTest extends CreatorCombinatorTest {
  /** {@inheritDoc} */
  @Override
  public void test() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();
    DD res1 = creator.makeNand(var1, creator.makeFalse());
    Assert.assertEquals(res1, creator.makeTrue());

    DD res2 = creator.makeNand(creator.makeFalse(), var2);
    Assert.assertEquals(res2, creator.makeTrue());
    creator.shutDown();
  }
}
