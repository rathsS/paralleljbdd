// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'IMPLY' test class uses {@link CreatorCombinatorTest} as base class to perform make 'IMPLY'
 * test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class ImplyTest extends CreatorCombinatorTest {
  /** {@inheritDoc} */
  @Override
  public void test() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeImply(var1, creator.makeTrue());
    Assert.assertEquals(res1, creator.makeTrue());

    DD res2 = creator.makeImply(creator.makeFalse(), var2);
    Assert.assertEquals(res2, creator.makeTrue());

    DD res3 = creator.makeImply(creator.makeTrue(), var2);
    assertEquals(res3, var2);
    creator.shutDown();
  }
}
