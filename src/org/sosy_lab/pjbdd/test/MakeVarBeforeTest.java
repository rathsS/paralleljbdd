// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'AND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'AND' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class MakeVarBeforeTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    checkVarIsInsertedCorrectly();
    checkRemainingOrderLeftUnchanged();
    checkCreatedDDsWithNewVarAreOnTop();
    checkCallMakeVarBeforeMultipleTimes();
    creator.shutDown();
  }

  private void checkCreatedDDsWithNewVarAreOnTop() {
    skipIfChained();

    DD var10 = creator.makeIthVar(10);
    DD varBefore10 = creator.makeVariableBefore(var10);

    DD and = creator.makeAnd(var10, varBefore10);

    assertEquals(varBefore10.getVariable(), and.getVariable());

    DD var5 = creator.makeIthVar(5);
    DD varBefore5 = creator.makeVariableBefore(var5);

    DD or = creator.makeAnd(var5, varBefore5);

    assertEquals(varBefore5.getVariable(), or.getVariable());
  }

  private void checkRemainingOrderLeftUnchanged() {
    int[] oldOrdering = creator.getVariableOrdering();
    DD var10 = creator.makeIthVar(10);
    DD varBefore10 = creator.makeVariableBefore(var10);
    int[] newOrdering = creator.getVariableOrdering();
    int iOld = 0;
    int iNew = 0;

    for (; iOld < oldOrdering.length; iNew++, iOld++) {
      if (newOrdering[iNew] == varBefore10.getVariable()) {
        iNew++;
      }
      assertEquals(oldOrdering[iOld], newOrdering[iNew]);
    }

    int[] oldOrdering2 = creator.getVariableOrdering();
    DD var0 = creator.makeIthVar(0);
    DD varBefore0 = creator.makeVariableBefore(var0);
    int[] newOrdering2 = creator.getVariableOrdering();
    int iOld2 = 0;
    int iNew2 = 0;

    for (; iOld2 < oldOrdering2.length; iNew2++, iOld2++) {
      if (newOrdering2[iNew2] == varBefore0.getVariable()) {
        iNew2++;
      }
      assertEquals(oldOrdering2[iOld2], newOrdering2[iNew2]);
    }
  }

  private void checkVarIsInsertedCorrectly() {
    DD var10 = creator.makeIthVar(10);
    DD varBefore10 = creator.makeVariableBefore(var10);

    int[] ordering = creator.getVariableOrdering();
    boolean insertedCorrectVar10 = false;
    for (int i = 0; i < ordering.length; i++) {
      if (ordering[i] == var10.getVariable()) {
        break;
      }
      if (ordering[i] == varBefore10.getVariable()) {
        insertedCorrectVar10 = ordering[i + 1] == var10.getVariable();
        break;
      }
    }

    assertTrue(insertedCorrectVar10);

    DD var5 = creator.makeIthVar(5);
    DD varBefore5 = creator.makeVariableBefore(var5);

    int[] ordering2 = creator.getVariableOrdering();
    boolean insertedCorrectVar5 = false;
    for (int i = 0; i < ordering2.length; i++) {
      if (ordering2[i] == var5.getVariable()) {
        break;
      }
      if (ordering2[i] == varBefore5.getVariable()) {
        insertedCorrectVar5 = ordering2[i + 1] == var5.getVariable();
        break;
      }
    }

    assertTrue(insertedCorrectVar5);

    DD var0 = creator.makeIthVar(0);
    DD varBefore0 = creator.makeVariableBefore(var0);

    int[] ordering3 = creator.getVariableOrdering();
    boolean insertedCorrectVar0 = false;
    for (int i = 0; i < ordering3.length; i++) {
      if (ordering3[i] == var0.getVariable()) {
        break;
      }
      if (ordering3[i] == varBefore0.getVariable()) {
        insertedCorrectVar0 = ordering3[i + 1] == var0.getVariable();
        break;
      }
    }

    assertTrue(insertedCorrectVar0);
  }

  private void checkCallMakeVarBeforeMultipleTimes() {
    int[] oldOrdering = creator.getVariableOrdering();
    DD var10 = creator.makeIthVar(10);
    DD varBefore10_1 = creator.makeVariableBefore(var10);
    DD varBefore10_2 = creator.makeVariableBefore(var10);
    DD varBefore10_3 = creator.makeVariableBefore(var10);
    DD varBefore10_4 = creator.makeVariableBefore(var10);

    int[] newOrdering = creator.getVariableOrdering();
    int iOld = 0;
    int iNew = 0;

    for (; iOld < oldOrdering.length; iNew++, iOld++) {
      if (newOrdering[iNew] == varBefore10_1.getVariable()) {
        assertEquals(newOrdering[++iNew], varBefore10_2.getVariable());
        assertEquals(newOrdering[++iNew], varBefore10_3.getVariable());
        assertEquals(newOrdering[++iNew], varBefore10_4.getVariable());
        assertEquals(newOrdering[++iNew], var10.getVariable());
      }
      assertEquals(oldOrdering[iOld], newOrdering[iNew]);
    }
  }
}
