// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Before;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.core.uniquetable.UniqueTable;
import org.sosy_lab.pjbdd.examples.Example;
import org.sosy_lab.pjbdd.examples.NQueens;
import org.sosy_lab.pjbdd.intBDD.IntUniqueTable;

/**
 * Test class which solves the {@link NQueens} example with various n for all known{@link Creator}
 * and {@link UniqueTable} / {@link IntUniqueTable} combinations.
 *
 * @author Stephan Holzner
 * @see NQueens
 * @see Creator
 * @see IntUniqueTable
 * @see UniqueTable
 * @since 1.0
 */
public class NQueensTest extends CreatorCombinatorTest {
  /** Size(N) to expected satCount Map. */
  private final Map<Integer, Integer> scenarios = new LinkedHashMap<>();

  /** Init all different scenario combinations. */
  @Before
  public void init() {
    scenarios.put(4, 2);
    scenarios.put(5, 10);
    scenarios.put(6, 4);
    scenarios.put(7, 40);
  }

  @Override
  public void test() {
    scenarios.forEach(
        (key, value) -> {
          Example example = new NQueens(key, creator);
          example.build();
          int satCount = example.solve().intValue();

          assertEquals(value.intValue(), satCount);
        });
    creator.shutDown();
  }
}
