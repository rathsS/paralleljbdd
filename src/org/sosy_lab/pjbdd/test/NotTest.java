// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'NOT' test class uses {@link CreatorCombinatorTest} as base class to perform make 'NOT' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class NotTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    DD bdd = creator.makeVariable();
    DD not = creator.makeNot(bdd);

    assertEquals(bdd.getHigh(), not.getLow());
    assertEquals(bdd.getLow(), not.getHigh());

    DD notZero = creator.makeNot(creator.makeFalse());
    Assert.assertEquals(creator.makeTrue(), notZero);

    DD notOne = creator.makeNot(creator.makeTrue());
    Assert.assertEquals(creator.makeFalse(), notOne);
    creator.shutDown();
  }
}
