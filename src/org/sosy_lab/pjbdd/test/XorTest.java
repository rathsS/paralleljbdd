// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'XOR' test class uses {@link CreatorCombinatorTest} as base class to perform make 'XOR' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class XorTest extends CreatorCombinatorTest {
  /** {@inheritDoc} */
  @Override
  public void test() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeXor(var1, var1);
    Assert.assertEquals(res1, creator.makeFalse());

    DD res2 = creator.makeXor(creator.makeFalse(), var2);
    assertEquals(res2, var2);

    DD res3 = creator.makeXor(var1, creator.makeFalse());
    assertEquals(res3, var1);
    creator.shutDown();
  }
}
