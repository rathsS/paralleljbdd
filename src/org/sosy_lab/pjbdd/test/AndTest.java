// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'AND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'AND' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class AndTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeAnd(var1, var1);
    assertEquals(res1, var1);

    DD res2 = creator.makeAnd(var1, creator.makeTrue());
    assertEquals(res2, var1);

    DD res3 = creator.makeAnd(var1, creator.makeFalse());
    Assert.assertEquals(res3, creator.makeFalse());

    DD res4 = creator.makeAnd(creator.makeFalse(), var1);
    Assert.assertEquals(res4, creator.makeFalse());

    DD res5 = creator.makeAnd(creator.makeTrue(), var2);
    assertEquals(res5, var2);

    DD res = creator.makeAnd(var1, var2);
    assertEquals(res.getVariable(), var1.getVariable());
    Assert.assertEquals(res.getLow(), creator.makeFalse());
    assertEquals(res.getHigh(), var2);
    creator.shutDown();
  }
}
