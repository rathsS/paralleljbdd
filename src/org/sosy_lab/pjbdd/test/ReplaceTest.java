// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'EXISTENTIAL QUANTIFICATION' test class uses {@link CreatorCombinatorTest} as base class to
 * perform make 'EXISTENTIAL QUANTIFICATION' test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class ReplaceTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    DD bdd0 = creator.makeIthVar(0);
    DD bdd1 = creator.makeIthVar(1);
    DD bdd2 = creator.makeIthVar(2);
    DD bdd3 = creator.makeIthVar(3);
    DD conjunction = creator.makeAnd(bdd0, bdd1);
    conjunction = creator.makeAnd(conjunction, creator.makeNot(bdd2));

    DD replace = creator.makeReplace(conjunction, bdd1, bdd3);

    DD expected = creator.makeAnd(bdd0, creator.makeNot(bdd2));
    expected = creator.makeAnd(expected, bdd3);

    assertEquals(expected, replace);

    creator.shutDown();
  }
}
