// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Assert;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.api.Statistics;
import org.sosy_lab.pjbdd.examples.NQueens;

/**
 * Make 'AND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'AND' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class SimpleStatsTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    skipIfChained();
    testSimpleCreatorStats();

    testSimpleBDDStats();
    testNQueensStats();
    creator.shutDown();
  }

  private void testSimpleCreatorStats() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();
    Creator.Stats creatorStats = Statistics.of(creator);

    // default nr of variables = 10
    assertEquals(12, creatorStats.getVariableCount());
    // pjbdd creates variable + it's negation + true&false
    assertEquals(26, creatorStats.getNodeCount());

    DD unused = creator.makeAnd(var1, var2);
    Creator.Stats creatorStats2 = Statistics.of(creator);

    assertEquals(27, creatorStats2.getNodeCount());
    assertEquals(1, creatorStats2.getCacheNodeCount());
    assertFalse(unused.isLeaf());
  }

  private void testSimpleBDDStats() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();
    Statistics.DDStats ddStats = Statistics.of(var1);

    assertEquals(3, ddStats.getNodeCount());
    assertEquals(2, ddStats.getEdgeCount());

    DD res3 = creator.makeAnd(var1, creator.makeFalse());
    Statistics.DDStats ddStats2 = Statistics.of(res3);

    Assert.assertEquals(1, ddStats2.getNodeCount());
    Assert.assertEquals(0, ddStats2.getEdgeCount());

    DD res = creator.makeAnd(var1, var2);
    Statistics.DDStats ddStats3 = Statistics.of(res);

    assertEquals(4, ddStats3.getNodeCount());
    assertEquals(4, ddStats3.getEdgeCount());
  }

  private void testNQueensStats() {
    NQueens nQueens = new NQueens(4, creator);
    nQueens.build();
    Statistics.DDStats ddStats1 = Statistics.of(nQueens.solution());
    assertEquals(31, ddStats1.getNodeCount());
    assertEquals(58, ddStats1.getEdgeCount());
  }
}
