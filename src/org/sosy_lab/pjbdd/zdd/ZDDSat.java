// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.zdd;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.bdd.BDDSat;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;

/**
 * ZDD based {@link SatAlgorithm} implementation. Uses serial algorithms for zdd sat operations and
 * {@link BDDSat} as base case since anySat implementations are equal.
 *
 * @author Stephan Holzner
 * @see SatAlgorithm
 * @see BDDSat
 * @since 1.1
 */
public class ZDDSat<V extends DD> extends BDDSat<V> {

  public ZDDSat(Cache<V, BigInteger> satCountCache, NodeManager<V> nodeManager) {
    super(satCountCache, nodeManager);
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(V b) {

    if (b.isTrue()) {
      return BigInteger.ONE;
    }
    if (b.isFalse()) {
      return BigInteger.ZERO;
    }

    BigInteger count = satCountCache.get(b);
    if (count != null) {
      return count;
    }

    BigInteger res = satCount(low(b)).add(satCount(high(b)));
    satCountCache.put(b, res);
    return res;
  }
}
