// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.cbdd;

import org.sosy_lab.pjbdd.api.DD;

/** Main bdd interface defining all cbdd methods. */
public interface CDD extends DD {

  int getChainEndVariable();

  @Override
  CDD getHigh();

  @Override
  CDD getLow();
}
