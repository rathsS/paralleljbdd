// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.examples;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Simple interface defining example problems which can be solved with bdd.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public interface Example {

  /** Builds the examples scenario. */
  void build();

  /**
   * Solve the example and return number of satisfying truth assignments.
   *
   * @return number of satisfying truth assignments.
   */
  BigInteger solve();

  /**
   * Get the examples solution as {@link DD}.
   *
   * @return the examples solution as {@link DD}
   */
  DD solution();

  /** close example. */
  void close();
}
