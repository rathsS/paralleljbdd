// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util.reference;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Custom {@link WeakReference} holding the referenced objects hashcode. {@link #hashCode()} and
 * {@link #equals(Object)} are overridden to make the reference itself equitable with it's
 * referenced object type.
 *
 * @author Stephan Holzner
 * @see WeakReference
 * @since 1.0
 */
public class ComparableWeakBDDReference<V extends DD> extends WeakReference<V> {
  /** the referenced objects hashcode. */
  private final int hashcode;

  /**
   * Creates new {@link ComparableWeakBDDReference} instances.
   *
   * @param referent - the referenced object
   * @param q - the queue the the reference should be added after referenced object's collection
   */
  public ComparableWeakBDDReference(V referent, ReferenceQueue<V> q) {
    super(referent, q);
    hashcode = referent.hashCode();
  }

  /**
   * Creates new {@link ComparableWeakBDDReference} instances.
   *
   * @param referent - the referenced object
   */
  public ComparableWeakBDDReference(V referent) {
    super(referent);
    hashcode = referent.hashCode();
  }

  /**
   * Get the referenced objects hashcode.
   *
   * @return the referenced objects hashcode
   */
  @Override
  public int hashCode() {
    return hashcode;
  }

  /**
   * Checks if the other object is equal to this or this' referenced object.
   *
   * @param o - the other object
   * @return <code>true</code> if both are {@link ComparableWeakBDDReference} holding a reference to
   *     the same object or other object is the referenced object, <code>false</code> else
   */
  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof ComparableWeakBDDReference) {
      return Objects.equals(((ComparableWeakBDDReference<?>) o).get(), get());
    } else {
      return Objects.requireNonNull(get()).equals(o);
    }
  }
}
