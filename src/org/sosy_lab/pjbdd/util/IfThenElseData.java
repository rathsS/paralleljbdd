// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util;

import org.sosy_lab.pjbdd.api.DD;

/**
 * Data class to represent a IfThenElse triple.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public class IfThenElseData {

  /** the getIf value. */
  private final DD ifBDD;

  /** the getThen value. */
  private final DD thenBDD;

  /** the getElse value. */
  private final DD elseBDD;

  /** the triples hashcode generated with it's containing objects hash codes. */
  private final int hashcode;

  /**
   * Creates new {@link IfThenElseData} instances.
   *
   * @param ifBDD - the if bdd branch
   * @param thenBDD - the then bdd branch
   * @param elseBDD - the else bdd branch
   */
  public IfThenElseData(DD ifBDD, DD thenBDD, DD elseBDD) {
    this.ifBDD = ifBDD;
    this.thenBDD = thenBDD;
    this.elseBDD = elseBDD;
    hashcode =
        HashCodeGenerator.generateHashCode(
            ifBDD.hashCode(), thenBDD.hashCode(), elseBDD.hashCode());
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object o) {
    if (!(o instanceof IfThenElseData)) {
      return false;
    }
    IfThenElseData other = (IfThenElseData) o;
    return other.getIf().equals(ifBDD)
        && other.getThen().equals(thenBDD)
        && other.getElse().equals(elseBDD);
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return hashcode;
  }

  /**
   * get the getIf value.
   *
   * @return the getIf value
   */
  public DD getIf() {
    return ifBDD;
  }

  /**
   * get the getThen value.
   *
   * @return the getThen value
   */
  public DD getThen() {
    return thenBDD;
  }

  /**
   * get the getElse value.
   *
   * @return the getElse value
   */
  public DD getElse() {
    return elseBDD;
  }
}
