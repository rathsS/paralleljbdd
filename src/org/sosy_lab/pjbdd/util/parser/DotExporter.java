// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util.parser;

import java.util.HashMap;
import java.util.Map;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.cbdd.CDD;
import org.sosy_lab.pjbdd.tbdd.tbddnode.TBDD;

/**
 * {@link Exporter} implementation which exports {@link DD} in Dot format.
 *
 * @author Stephan Holzner
 * @see Exporter
 * @since 1.0
 */
public class DotExporter implements Exporter<DD> {

  /** Cash used for unique bdd node enumeration. */
  private final Map<DD, Integer> bddEnumeration = new HashMap<>();

  /** {@inheritDoc} */
  @Override
  public String extension() {
    return ".dot";
  }

  /** {@inheritDoc} */
  @Override
  public String bddToString(DD node) {
    String result = "digraph G {\n";
    result += recursiveBddToDotStrings(node).toString();
    result += "}";
    bddEnumeration.clear();
    return result;
  }

  /**
   * Recursive fills {@link StringBuilder} with bdd nodes child String representations. Afterwards
   * nodes String representation will be appended.
   *
   * @param node - the node
   * @return the nodes String representation as {@link StringBuilder}
   */
  private StringBuilder recursiveBddToDotStrings(DD node) {

    StringBuilder result = new StringBuilder();
    bddEnumeration.putIfAbsent(node, bddEnumeration.size());

    if (!node.isLeaf()) {
      if (!bddEnumeration.containsKey(node.getLow())) {
        result.append(recursiveBddToDotStrings(node.getLow()));
      }
      if (!bddEnumeration.containsKey(node.getHigh())) {
        result.append(recursiveBddToDotStrings(node.getHigh()));
      }
    }
    if (node instanceof TBDD) {
      return result.append(nodeToStringTBDD((TBDD) node)).append("\n");
    } else {
      return result.append(nodeToString(node)).append("\n");
    }
  }

  /**
   * converts a bdd node to .dot string.
   *
   * @param node - a bdd
   * @return bdd as .dot string
   */
  private String nodeToString(DD node) {
    if (node.isFalse()) {
      int index = bddEnumeration.get(node);
      return index + "[label=" + 0 + "]";
    }
    if (node.isTrue()) {
      int index = bddEnumeration.get(node);
      return index + "[label=" + 1 + "]";
    }
    int index = bddEnumeration.get(node);

    String label = "x" + node.getVariable();

    if (node instanceof CDD) {
      CDD cbddNode = (CDD) node;
      if (cbddNode.getVariable() != cbddNode.getChainEndVariable()) {
        label += "x" + cbddNode.getChainEndVariable();
      }
    }

    return index
        + "[label="
        + label
        + "] \n"
        + index
        + "->"
        + bddEnumeration.get(node.getLow())
        + "[style=dashed]  \n"
        + index
        + "->"
        + bddEnumeration.get(node.getHigh());
  }

  /** Variation of the noteToString function for TBDDs. * */
  private String nodeToStringTBDD(TBDD node) {
    if (node.isFalse()) {
      int index = bddEnumeration.get(node);
      return index + "[label=" + 0 + "]";
    }
    if (node.isTrue()) {
      int index = bddEnumeration.get(node);
      return index + "[label=" + 1 + "]";
    }
    int index = bddEnumeration.get(node);

    String label = "x" + node.getVariable();
    String lowTag = "x" + node.getLowTag();
    String highTag = "x" + node.getHighTag();

    if (node.getLowTag() == -3) {
      lowTag = "⊥";
    }

    if (node.getHighTag() == -3) {
      highTag = "⊥";
    }

    // if (node.getVariable() != node.getChainEndVariable()) {
    // label += "x" + node.getChainEndVariable();
    // }

    return index
        + "[label="
        + label
        + "] \n"
        + index
        + "->"
        + bddEnumeration.get(node.getLow())
        + "[style=dashed, label=\""
        + lowTag
        + "\"]  \n"
        + index
        + "->"
        + bddEnumeration.get(node.getHigh())
        + "[label=\""
        + highTag
        + "\"]";
  }
}
