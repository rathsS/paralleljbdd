// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd.algorithm;

import java.util.Optional;
import org.sosy_lab.pjbdd.api.Creator;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * abstract {@link ITEBDDAlgorithm} implementation with {@link DD} objects as bdd data structure.
 * Base class for several concurrent ITEBDDAlgorithm implementations Subclasses:
 *
 * <ul>
 *   <li>{@link FutureITEAlgorithm}
 *   <li>{@link StreamITEAlgorithm}
 *   <li>{@link ForkJoinITEAlgorithm}
 * </ul>
 *
 * @author Stephan Holzner
 * @see Creator
 * @see FutureITEAlgorithm
 * @see StreamITEAlgorithm
 * @see ForkJoinITEAlgorithm
 * @see ITEBDDAlgorithm
 * @since 1.0
 */
public abstract class ParallelITEAlgorithm<V extends DD> extends ITEBDDAlgorithm<V> {
  /** Worker thread pool manager. */
  protected final ParallelismManager parallelismManager;

  /**
   * Creates new {@link ParallelITEAlgorithm} instances with given parameters.
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the node manager
   * @param parallelismManager - the parallelism manager
   */
  protected ParallelITEAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager<V> nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    this.parallelismManager = parallelismManager;
  }

  /**
   * Asynchronous 'ITE' calculation for input triple.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return ite(f1, f2, f3)
   */
  @Override
  public V makeIte(V f1, V f2, V f3) {
    Optional<V> check = terminalIteCheck(f1, f2, f3);
    if (check.isPresent()) {
      return check.get();
    }

    int topVar = topVar(level(f1), level(f2), level(f3));
    V res;
    V low = null;
    V high = null;
    V lowF1 = low(f1, topVar);
    V lowF2 = low(f2, topVar);
    V lowF3 = low(f3, topVar);
    V highF1 = high(f1, topVar);
    V highF2 = high(f2, topVar);
    V highF3 = high(f3, topVar);

    Optional<V> lowCheck;
    Optional<V> highCheck;
    lowCheck = terminalIteCheck(lowF1, lowF2, lowF3);
    highCheck = terminalIteCheck(highF1, highF2, highF3);

    if (lowCheck.isPresent()) {
      low = lowCheck.get();
    }
    if (highCheck.isPresent()) {
      high = highCheck.get();
    }

    if (forkCheck(low, high, topVar)) {
      res = forkITE(lowF1, lowF2, lowF3, highF1, highF2, highF3, topVar);
    } else {
      if (low == null) {
        low = makeIte(lowF1, lowF2, lowF3);
      }
      if (high == null) {
        high = makeIte(highF1, highF2, highF3);
      }
      res = makeNode(low, high, topVar);
    }
    this.cacheItem(f1, f2, f3, res);
    return res;
  }

  /**
   * Performs parallel recursion step.
   *
   * @param lowF1 - f1 low branch cofactor
   * @param lowF2 - f2 low branch cofactor
   * @param lowF3 - f3 low branch cofactor
   * @param highF1 - f1 high branch cofactor
   * @param highF2 - f2 high branch cofactor
   * @param highF3 - f3 high branch cofactor
   * @param topVar - ite recursions top var
   * @return makeNode(ite ( lowF1, lowF2, lowF3), ite(highF1,highF2, highF3),topVar)
   */
  protected abstract V forkITE(V lowF1, V lowF2, V lowF3, V highF1, V highF2, V highF3, int topVar);

  /**
   * Check if forkITE to be called.
   *
   * @param low - terminal checked low branch
   * @param high - terminal checked high branch
   * @param topVar - ite recursions top var
   * @return true if 'forkITE' will be called in current recursion step false else
   */
  private boolean forkCheck(V low, V high, int topVar) {
    return parallelismManager.canFork(topVar) && high == null && low == null;
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    parallelismManager.shutdown();
  }
}
